package main

import (
	"bufio"
	"fmt"
	"os"

	"gitlab.com/Kucho/edulist/config"
	"gitlab.com/Kucho/edulist/functions"
)

func main() {

	rutas, err := config.CheckStops()
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Ingrese los nombres de las listas a transformar (separado por espacios):")

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanWords)

	// Recorre las palabras ingresadas
	for scanner.Scan() {
		functions.ProcesarLibros(scanner.Text(), rutas)
	}
}
