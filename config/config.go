package config

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path"
	"regexp"
	"strings"
)

// Paradas operada por EduExpress
type Paradas struct {
	Nombre string
	Orden  int
}

// Ruta es un conjunto de paradas
type Ruta struct {
	Nombre  string
	Paradas []Paradas
}

// CheckStops valida que exista la carpeta de las paradas y existan datos
func CheckStops() ([]Ruta, error) {

	var Rutas []Ruta

	files, err := ioutil.ReadDir("./stops")
	if err != nil {
		fmt.Println("No existe la carpeta 'stops' en el directorio.")
		return nil, err
	}

	for _, file := range files {

		var nombreRuta string

		if path.Ext(file.Name()) == ".csv" {
			// Si es csv, debe ser el archivo
			csvFile, _ := os.Open("./stops/" + file.Name())
			reader := csv.NewReader(bufio.NewReader(csvFile))

			var ruta []Paradas

			c := 0

			for {
				line, error := reader.Read()
				if error == io.EOF {
					break
				} else if error != nil {
					log.Fatal(error)
				}

				if c == 0 {
					rp := regexp.MustCompile(`[A-zÀ-ú]+`)
					a := rp.FindAllString(line[0], -1)

					nombreRuta = strings.Title(strings.ToLower(strings.Join(a, " ")))
				}

				if c != 0 {
					ruta = append(ruta, Paradas{
						Orden:  c,
						Nombre: strings.Title(strings.ToLower(line[0])),
					})
				}
				c++
			}

			Rutas = append(Rutas, Ruta{
				Nombre:  nombreRuta,
				Paradas: ruta,
			})
		}

		fmt.Println(nombreRuta + " cargado desde el archivo " + file.Name())
	}
	return Rutas, err
}
