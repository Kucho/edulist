package functions

import (
	"fmt"
	"regexp"
	"strings"

	"github.com/360EntSecGroup-Skylar/excelize"
	"gitlab.com/Kucho/edulist/config"
	"gitlab.com/Kucho/edulist/model"
)

// ProcesarLibros extrae toda la información y la transforma
func ProcesarLibros(name string, rutas []config.Ruta) {

	xlsx, err := excelize.OpenFile(name + ".xlsx")
	if err != nil {
		fmt.Println("No se pudo abrir el archivo con nombre " + name + ".xlsx")
		return
	}

	sheetname := "sheet1"
	var ListaConfig model.ListaConfig

	// Validar los campos más importantes
	// Dame el valor de la ruta
	ListaConfig.Ruta = xlsx.GetCellValue(sheetname, "C1")
	if ListaConfig.Ruta == "" {
		fmt.Println("Ruta no especificada en el archivo. Revise y vuelva a intentarlo.")
		return
	}
	ListaConfig.Ruta = strings.Trim(strings.Title(strings.ToLower(ListaConfig.Ruta)), "")

	// Dame el trayecto de la ruta
	ListaConfig.Trayecto = xlsx.GetCellValue(sheetname, "E1")
	if ListaConfig.Trayecto == "" {
		fmt.Println("Trayecto de la ruta no especificado en el archivo. Revise y vuelva a intentarlo.")
		return
	}
	ListaConfig.Trayecto = strings.Title(strings.ToLower(ListaConfig.Trayecto))

	// Dame la nave de la ruta
	ListaConfig.Nave = xlsx.GetCellValue(sheetname, "I1")
	if ListaConfig.Nave == "" {
		fmt.Println("Nave para el viaje no especificado en el archivo. Revise y vuelva a intentarlo.")
		return
	}
	ListaConfig.Nave = strings.ToTitle(ListaConfig.Nave)

	// Dame la fecha de la ruta
	ListaConfig.Fecha = xlsx.GetCellValue(sheetname, "G1")
	if ListaConfig.Fecha == "" {
		fmt.Println("Fecha del viaje no especificado en el archivo. Revise y vuelva a intentarlo.")
		return
	}

	// Dame la placa de la nave
	ListaConfig.Placa = xlsx.GetCellValue(sheetname, "K1")
	if ListaConfig.Placa == "" {
		fmt.Println("Placa de la nave no especificado en el archivo. Revise y vuelva a intentarlo.")
		return
	}
	ListaConfig.Placa = strings.ToTitle(ListaConfig.Placa)

	rutaIndex := -1

	// Transformar toda la data, sin el formato horrible del sistema
	rp := regexp.MustCompile(`[A-zÀ-ú]+`)
	rpp := regexp.MustCompile(`\d{2}-\d{2}-\d{4}`)
	rppp := regexp.MustCompile(`[a-zA-Z0-9/-]+`)
	a := rp.FindAllString(ListaConfig.Ruta, -1)
	ListaConfig.Ruta = strings.Join(a, " ")
	a = rp.FindAllString(ListaConfig.Trayecto, -1)
	ListaConfig.Trayecto = strings.Join(a, " ")
	a = rp.FindAllString(ListaConfig.Nave, -1)
	ListaConfig.Nave = strings.Join(a, " ")
	a = rppp.FindAllString(ListaConfig.Placa, -1)
	ListaConfig.Placa = strings.Join(a, " ")
	a = rpp.FindAllString(ListaConfig.Fecha, -1)
	ListaConfig.Fecha = strings.Join(a, "")

	for i := 0; i < len(rutas); i++ {
		if rutas[i].Nombre == ListaConfig.Ruta {
			rutaIndex = i
			break
		}
	}

	if rutaIndex == -1 {
		fmt.Println("Nombre de la ruta no fue encontrado en el archivo de configuración")
		return
	}

	fmt.Println("Párametros OK. Empezando la transformación...")

	var rutaSuben []model.ResumenPasajero
	var rutaBajan []model.ResumenPasajero
	var rutaDirecta []model.ResumenPasajero

	switch ListaConfig.Trayecto {
	case "Surcada":
		for i, j := 0, len(rutas[rutaIndex].Paradas)-1; i < j; i, j = i+1, j-1 {
			rutas[rutaIndex].Paradas[i], rutas[rutaIndex].Paradas[j] = rutas[rutaIndex].Paradas[j], rutas[rutaIndex].Paradas[i]
		}
		fallthrough

	case "Bajada":
		rows := xlsx.GetRows(sheetname)

		for i := 0; i < len(rutas[rutaIndex].Paradas); i++ {

			var pasajero model.Pasajero

			var pasajerosSuben []model.Pasajero
			var pasajerosBajan []model.Pasajero
			var pasajerosDirecto []model.Pasajero

			for c := 2; c < len(rows); c++ {

				// Extraemos el valor del origen sin el formato horrible del sistema
				origen := strings.Title(strings.ToLower(rows[c][7]))
				a := rp.FindAllString(origen, -1)
				origen = strings.Join(a, " ")

				// Extraemos el valor del destino sin el formato horrible del sistema
				destino := strings.Title(strings.ToLower(rows[c][8]))
				a = rp.FindAllString(destino, -1)
				destino = strings.Join(a, " ")

				pasajero = model.Pasajero{
					Nombres:      rows[c][1],
					Apellidos:    rows[c][2],
					Edad:         rows[c][3],
					Sexo:         rows[c][4],
					Nacionalidad: rows[c][5],
					Documento:    rows[c][6],
					Origen:       rows[c][7],
					Destino:      rows[c][8],
					Comprobante:  rows[c][9],
					Monto:        rows[c][10],
				}

				if (i == 0) && (origen == rutas[rutaIndex].Paradas[0].Nombre) && (destino == rutas[rutaIndex].Paradas[len(rutas[rutaIndex].Paradas)-1].Nombre) {
					pasajerosDirecto = append(pasajerosDirecto, pasajero)
					continue
				}

				if destino != rutas[rutaIndex].Paradas[i].Nombre && origen == rutas[rutaIndex].Paradas[i].Nombre {
					pasajerosSuben = append(pasajerosSuben, pasajero)
					continue
				}

				if origen != rutas[rutaIndex].Paradas[i].Nombre && destino == rutas[rutaIndex].Paradas[i].Nombre {
					pasajerosBajan = append(pasajerosBajan, pasajero)
				}

			}

			rutaSuben = append(rutaSuben, model.ResumenPasajero{Nombre: rutas[rutaIndex].Paradas[i].Nombre, Cliente: pasajerosSuben})
			rutaBajan = append(rutaBajan, model.ResumenPasajero{Nombre: rutas[rutaIndex].Paradas[i].Nombre, Cliente: pasajerosBajan})
			rutaDirecta = append(rutaDirecta, model.ResumenPasajero{Nombre: rutas[rutaIndex].Paradas[i].Nombre, Cliente: pasajerosDirecto})

		}
		fmt.Println("Lista: " + ListaConfig.Nave + " - " + ListaConfig.Ruta + " - " + ListaConfig.Fecha + " --> PROCESADO")
		/*
			// A este punto ya tengo separado los slices. Todo en orden. Lo que queda es mostrar el resumen.
			w := tabwriter.NewWriter(os.Stdout, 0, 0, 5, ' ', 0)
			defer w.Flush()

			fmt.Fprintln(w, "Parada\tSuben\tBajan")
			fmt.Fprintln(w, "----------\t----------\t----------")
			c := 0
			for i := range rutas[rutaIndex].Paradas {
				if c == 0 {
					fmt.Fprintf(w, "%s\t%s\t%s\t\n", rutas[rutaIndex].Paradas[i].Nombre, strconv.Itoa(len(rutaDirecta[i].Cliente)+len(rutaSuben[i].Cliente)), "0")
				} else if c == len(rutas[rutaIndex].Paradas)-1 {
					fmt.Fprintf(w, "%s\t%s\t%s\t\n", rutas[rutaIndex].Paradas[i].Nombre, "0", strconv.Itoa(len(rutaDirecta[i].Cliente)+len(rutaBajan[i].Cliente)))
				} else {
					fmt.Fprintf(w, "%s\t%s\t%s\t\n", rutas[rutaIndex].Paradas[i].Nombre, strconv.Itoa(len(rutaSuben[i].Cliente)), strconv.Itoa(len(rutaBajan[i].Cliente)))
				}
				c++
			}
		*/

	default:
		fmt.Println("Trayecto no definido")
		return

	}

	ExportarDestino(ListaConfig, rutaDirecta, rutaSuben, rutaBajan)
	ExportarOrigen(ListaConfig, rutaDirecta, rutaSuben, rutaBajan)
}
