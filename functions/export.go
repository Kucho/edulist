package functions

import (
	"fmt"
	"strconv"
	"strings"

	//Encode del logo de la empresa
	_ "image/png"

	"github.com/360EntSecGroup-Skylar/excelize"
	"gitlab.com/Kucho/edulist/model"
)

const (
	// estiloCuerpo es el JSON para darle vida a los encabezados
	estiloCuerpo = `"font":{"family":"Arial","size":11,"color":"#000000"}`
	// estiloEncabezado es el JSON para darle vida a los encabezados
	estiloEncabezado = `"font":{"bold":true,"family":"Arial","size":11,"color":"#000000"}`
	// estiloTitulo es el JSON para darle vida al título del archivo
	estiloTitulo = `"font":{"bold":true,"family":"Arial","size":14,"color":"#000000","underline":"sng"}`
	// estiloBorde es el JSON para darle vida a los cuadros
	estiloBorde = `"border":[{"type":"left","color":"#000000","style":1},{"type":"top","color":"#000000","style":1},{"type":"bottom","color":"#000000","style":1},{"type":"right","color":"#000000","style":1}]`
	// estiloCentrar  es el JSON para anilear los cuadros centrados
	estiloCentrar = `"alignment":{"horizontal":"center","ident":1,"justify_last_line":true,"reading_order":0,"relative_indent":1,"shrink_to_fit":true,"vertical":"center","wrap_text":true}`
	// EstiloEncabezado es el JSON para darle vida a los encabezados
	estiloCentrarSimple = `"alignment":{"horizontal":"center","vertical":"center"}`
)

func setHeader(excel *excelize.File, config model.ListaConfig, sheet, title string) {

	// Ponemos el título chévere
	excel.SetCellValue(sheet, "C1", title)

	titlestyle, err := excel.NewStyle("{" + estiloTitulo + "," + estiloBorde + "," + estiloCentrar + "}")
	if err != nil {
		fmt.Println(err)
	}
	excel.SetCellStyle(sheet, "C1", "H1", titlestyle)
	excel.MergeCell(sheet, "C1", "H1")

	// Ponemos los datos del viaje
	datastyle, err := excel.NewStyle("{" + estiloEncabezado + "," + estiloBorde + "," + estiloCentrar + "}")
	if err != nil {
		fmt.Println(err)
	}
	excel.SetCellValue(sheet, "C2", "Embarcación:  "+config.Nave+"   |   Placa:  "+config.Placa+"   |   "+config.Ruta)
	excel.SetCellStyle(sheet, "C2", "H2", datastyle)
	excel.MergeCell(sheet, "C2", "H2")
	excel.SetCellValue(sheet, "C3", "Trayecto: "+config.Trayecto+"   |   Fecha: "+config.Fecha)
	excel.SetCellStyle(sheet, "C3", "H3", datastyle)
	excel.MergeCell(sheet, "C3", "H3")

	// Ponemos valores de las cabeceras
	excel.SetCellValue(sheet, "A4", "Nº")
	excel.SetCellValue(sheet, "B4", "Apellidos")
	excel.SetCellValue(sheet, "C4", "Nombres")
	excel.SetCellValue(sheet, "D4", "Edad")
	excel.SetCellValue(sheet, "E4", "Documento")
	excel.SetCellValue(sheet, "F4", "Nacionalidad")
	excel.SetCellValue(sheet, "G4", "Origen")
	excel.SetCellValue(sheet, "H4", "Destino")

	// Establecemos tamaño
	// Columna N
	excel.SetColWidth(sheet, "A", "A", 5.0)
	// Columnas Apellidos y Nombres
	excel.SetColWidth(sheet, "B", "C", 30.0)
	// Columna Edad
	excel.SetColWidth(sheet, "D", "D", 7.0)
	// Columnas Documento y Nacionalidad
	excel.SetColWidth(sheet, "E", "F", 15.0)
	// Columnas Origen y Destino
	excel.SetColWidth(sheet, "G", "H", 20.0)

	// Ponemos el estilo de los encabezados
	headerStyle, err := excel.NewStyle("{" + estiloEncabezado + "," + estiloBorde + "," + estiloCentrarSimple + "}")
	if err != nil {
		fmt.Println(err)
	}
	excel.SetCellStyle(sheet, "A4", "H4", headerStyle)

	// Agregamos el logo final, la cereza
	excel.MergeCell(sheet, "A1", "B3")
	err = excel.AddPicture(sheet, "A1", "./assets/logo.png", `{"x_offset": 5, "y_offset": 10,"hyperlink": "https://www.eduexpress.pe", "hyperlink_type": "External", "print_obj": true, "lock_aspect_ratio": false, "locked": false}`)
	if err != nil {
		fmt.Println(err)
	}
}

// ExportarOrigen exporta el libro con todas las especificaciones y requisitos de capitanía
func ExportarOrigen(config model.ListaConfig, directo, suben, bajan []model.ResumenPasajero) {

	// Crea un nuevo libro
	xlsx := excelize.NewFile()

	sheetname := "Capitanía"

	// Renombramos la hoja
	xlsx.SetSheetName("Sheet1", sheetname)

	// Ponemos el header
	setHeader(xlsx, config, sheetname, "LISTA DE ZARPE - ORIGEN")

	start := 5
	c := 0
	d := 0

	for _, ruta := range directo {
		for j, pasajero := range ruta.Cliente {
			xlsx.SetCellValue(sheetname, "A"+strconv.Itoa(start+j), strconv.Itoa(c+1))
			xlsx.SetCellValue(sheetname, "B"+strconv.Itoa(start+j), strings.Title(strings.ToLower(pasajero.Apellidos)))
			xlsx.SetCellValue(sheetname, "C"+strconv.Itoa(start+j), strings.Title(strings.ToLower(pasajero.Nombres)))
			xlsx.SetCellValue(sheetname, "D"+strconv.Itoa(start+j), strings.Title(strings.ToLower(pasajero.Edad)))
			xlsx.SetCellValue(sheetname, "E"+strconv.Itoa(start+j), strings.Title(strings.ToLower(pasajero.Documento)))
			xlsx.SetCellValue(sheetname, "F"+strconv.Itoa(start+j), strings.Title(strings.ToLower(pasajero.Nacionalidad)))
			xlsx.SetCellValue(sheetname, "G"+strconv.Itoa(start+j), strings.Title(strings.ToLower(pasajero.Origen)))
			xlsx.SetCellValue(sheetname, "H"+strconv.Itoa(start+j), strings.Title(strings.ToLower(pasajero.Destino)))
			c++
		}
	}

	for i := 0; i < len(suben[0].Cliente); i++ {
		if suben[0].Cliente[i].Destino != suben[len(bajan)-1].Nombre {
			xlsx.SetCellValue(sheetname, "A"+strconv.Itoa(start+c+d), strconv.Itoa(c+d+1))
			xlsx.SetCellValue(sheetname, "B"+strconv.Itoa(start+c+d), strings.Title(strings.ToLower(suben[0].Cliente[i].Apellidos)))
			xlsx.SetCellValue(sheetname, "C"+strconv.Itoa(start+c+d), strings.Title(strings.ToLower(suben[0].Cliente[i].Nombres)))
			xlsx.SetCellValue(sheetname, "D"+strconv.Itoa(start+c+d), strings.Title(strings.ToLower(suben[0].Cliente[i].Edad)))
			xlsx.SetCellValue(sheetname, "E"+strconv.Itoa(start+c+d), strings.Title(strings.ToLower(suben[0].Cliente[i].Documento)))
			xlsx.SetCellValue(sheetname, "F"+strconv.Itoa(start+c+d), strings.Title(strings.ToLower(suben[0].Cliente[i].Nacionalidad)))
			xlsx.SetCellValue(sheetname, "G"+strconv.Itoa(start+c+d), strings.Title(strings.ToLower(suben[0].Cliente[i].Origen)))
			xlsx.SetCellValue(sheetname, "H"+strconv.Itoa(start+c+d), strings.Title(strings.ToLower(suben[0].Cliente[i].Destino)))
			d++
		}
	}

	dataCentrada, err := xlsx.NewStyle("{" + estiloCuerpo + "," + estiloBorde + "," + estiloCentrarSimple + "}")
	if err != nil {
		fmt.Println(err)
	}
	dataSincentrar, err := xlsx.NewStyle("{" + estiloCuerpo + "," + estiloBorde + "}")
	if err != nil {
		fmt.Println(err)
	}

	xlsx.SetCellStyle(sheetname, "A5", "C"+strconv.Itoa(start+c+d-1), dataSincentrar)
	xlsx.SetCellStyle(sheetname, "D5", "H"+strconv.Itoa(start+c+d-1), dataCentrada)

	// Guardamos el libro de acuerdo a la configuración dada
	err = xlsx.SaveAs("./" + config.Nave + "-" + config.Fecha + "-Origen.xlsx")
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Se creó el archivo: " + config.Nave + "-" + config.Fecha + "-Origen.xlsx")
}

// ExportarDestino exporta el libro con todas las especificaciones y requisitos de capitanía
func ExportarDestino(config model.ListaConfig, directo, suben, bajan []model.ResumenPasajero) {

	// Crea un nuevo libro
	xlsx := excelize.NewFile()

	sheetname := "Capitanía"

	// Renombramos la hoja
	xlsx.SetSheetName("Sheet1", sheetname)

	// Ponemos el header
	setHeader(xlsx, config, sheetname, "LISTA DE ZARPE - DESTINO")

	start := 5

	for i := 0; i < len(bajan[len(bajan)-1].Cliente); i++ {
		xlsx.SetCellValue(sheetname, "A"+strconv.Itoa(start+i), strconv.Itoa(i+1))
		xlsx.SetCellValue(sheetname, "B"+strconv.Itoa(start+i), strings.Title(strings.ToLower(bajan[len(bajan)-1].Cliente[i].Apellidos)))
		xlsx.SetCellValue(sheetname, "C"+strconv.Itoa(start+i), strings.Title(strings.ToLower(bajan[len(bajan)-1].Cliente[i].Nombres)))
		xlsx.SetCellValue(sheetname, "D"+strconv.Itoa(start+i), strings.Title(strings.ToLower(bajan[len(bajan)-1].Cliente[i].Edad)))
		xlsx.SetCellValue(sheetname, "E"+strconv.Itoa(start+i), strings.Title(strings.ToLower(bajan[len(bajan)-1].Cliente[i].Documento)))
		xlsx.SetCellValue(sheetname, "F"+strconv.Itoa(start+i), strings.Title(strings.ToLower(bajan[len(bajan)-1].Cliente[i].Nacionalidad)))
		xlsx.SetCellValue(sheetname, "G"+strconv.Itoa(start+i), strings.Title(strings.ToLower(bajan[len(bajan)-1].Cliente[i].Origen)))
		xlsx.SetCellValue(sheetname, "H"+strconv.Itoa(start+i), strings.Title(strings.ToLower(bajan[len(bajan)-1].Cliente[i].Destino)))
	}

	dataCentrada, err := xlsx.NewStyle("{" + estiloCuerpo + "," + estiloBorde + "," + estiloCentrarSimple + "}")
	if err != nil {
		fmt.Println(err)
	}
	dataSincentrar, err := xlsx.NewStyle("{" + estiloCuerpo + "," + estiloBorde + "}")
	if err != nil {
		fmt.Println(err)
	}

	xlsx.SetCellStyle(sheetname, "A5", "C"+strconv.Itoa(start+len(bajan[len(bajan)-1].Cliente)-1), dataSincentrar)
	xlsx.SetCellStyle(sheetname, "D5", "H"+strconv.Itoa(start+len(bajan[len(bajan)-1].Cliente)-1), dataCentrada)

	// Guardamos el libro de acuerdo a la configuración dada
	err = xlsx.SaveAs("./" + config.Nave + "-" + config.Fecha + "-Destino.xlsx")
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Se creó el archivo: " + config.Nave + "-" + config.Fecha + "-Destino.xlsx")
}
