package model

// Pasajero contiene toda la información del viajero
type Pasajero struct {
	Nombres      string
	Apellidos    string
	Edad         string
	Sexo         string
	Nacionalidad string
	Documento    string
	Origen       string
	Destino      string
	Comprobante  string
	Monto        string
}

// ResumenPasajero muestra los clientes de la parada
type ResumenPasajero struct {
	Nombre  string
	Cliente []Pasajero
}
