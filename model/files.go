package model

// ListaConfig muestra toda la configuración del archivo de la lista
type ListaConfig struct {
	Fecha    string
	Ruta     string
	Trayecto string
	Nave     string
	Placa    string
}
